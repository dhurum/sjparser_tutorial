# SJParser tutorial

This repo contains a tutorial for the [SJParser](https://gitlab.com/dhurum/sjparser) library.

## Problem

Suppose that you are a software engineer, working for a king. He has an army, and he keeps track of it using Army Tracker 3000 system. Now he wants to make a nice big scroll with listing of his units, but the Army Tracker 3000 does not have this functionality. So he ordered you to do this listing.

The army tracking system allows you to export data in JSON.  
The document is structured like this:

Root is an object with two members:

- `king` - A mandatory object with two members:
    * `name` - A mandatory string with king's name;
    * `title` - A mandatory string with king's title;
- `army` - A mandatory array of objects, describing units, with these members:
    * `name` - A mandatory string with unit's name;
    * `equipment`- A mandatory array of objects, describing unit's equipment, with these members:
        - `name` - A mandatory string with item's name;
        - `price` - A mandatory integer with item's price in gold;
    * `years of service` - A mandatory double with a number of years in service;
    * `officer` - A mandatory boolean, true if the unit is an officer;
    * `awards` - An optional array of objects, describing unit's awards, with these members:
        - `name` - A mandatory string with award's name;
        - `year` - A mandatory integer with award's issue year;
        - `reason` - A mandatory string with award's issue reason;
    * `type` - A mandatory string with type of the unit. Depending on it's value the next fiels will be:
        - `"infantry"`:
            * `skills` - An optional array of strings with skill names;
        - `"mage"`:
            * `spells` - An optional array of strings with spell names;
        - `"cavalry"`:
            * `skills` - An optional array of strings with skill names;
            * `mount`: - A mandatory object describing a mount, with these members:
                - `type` - A mandatory string with mount's type.  Depending on it's value next fiels will be:
                    * `"horse"`:
                        - `color` - A mandatory string with horse's color;
                    * `"bear"`:
                        - `fangs length` - A mandatory double with bear's fangs length;

You need to write a program that will print all this information in a human-readable form.

## Tutorial

[Part 1](https://gitlab.com/dhurum/sjparser_tutorial/tree/master/Part%201)  
[Part 2](https://gitlab.com/dhurum/sjparser_tutorial/tree/master/Part%202)  
[Part 3](https://gitlab.com/dhurum/sjparser_tutorial/tree/master/Part%203)  
[Part 4](https://gitlab.com/dhurum/sjparser_tutorial/tree/master/Part%204)  
[Part 5](https://gitlab.com/dhurum/sjparser_tutorial/tree/master/Part%205)  

## Cloning

This repository has git submodules, so you need to clone it with command `git clone --recursive https://gitlab.com/dhurum/sjparser_tutorial`
