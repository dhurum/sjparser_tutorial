cmake_minimum_required(VERSION 3.8)

project(printarmy)

add_subdirectory(sjparser)

add_executable(printarmy 
  army_printer.cpp
  printarmy.cpp
)

target_compile_options(printarmy PRIVATE
  -Werror
  -Wall
  -Wextra
  -Wpedantic
)

target_link_libraries(printarmy sjparser)
