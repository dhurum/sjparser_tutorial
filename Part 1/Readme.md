# Part 1

# King information parser

## Preparations

We will have our code in three files, [army_printer.h](https://gitlab.com/dhurum/sjparser_tutorial/blob/master/Part%201/army_printer.h), [army_printer.cpp](https://gitlab.com/dhurum/sjparser_tutorial/blob/master/Part%201/army_printer.cpp) and [printarmy.cpp](https://gitlab.com/dhurum/sjparser_tutorial/blob/master/Part%201/printarmy.cpp), plus a [CMakeLists.txt](https://gitlab.com/dhurum/sjparser_tutorial/blob/master/Part%201/CMakeLists.txt) so let's initialize a git repo and add our files:

~~~bash
touch army_printer.h army_printer.cpp printarmy.cpp CMakeLists.txt
git init
git add army_printer.h army_printer.cpp printarmy.cpp CMakeLists.txt
~~~

Now, it's time to add SJParser as a submodule:

~~~bash
git submodule add https://gitlab.com/dhurum/sjparser.git
~~~

Then let's make a simple [CMakeLists.txt](https://gitlab.com/dhurum/sjparser_tutorial/blob/master/Part%201/CMakeLists.txt):

~~~cmake
cmake_minimum_required(VERSION 3.8)

project(printarmy)

add_subdirectory(sjparser)

add_executable(printarmy 
  army_printer.cpp
  printarmy.cpp
)

target_compile_options(printarmy PRIVATE
  -Werror
  -Wall
  -Wextra
  -Wpedantic
)

target_link_libraries(printarmy sjparser)
~~~

As you can see, all you need to do when you use SJParser with CMake is to write `add_subdirectory` and `target_link_libraries`.

## Boilerplate

Let's start by parsing information about the king.

First we need to write a header file with our parser function declaration ([army_printer.h](https://gitlab.com/dhurum/sjparser_tutorial/blob/master/Part%201/army_printer.h)):

~~~cpp
#pragma once

#include <string>

bool printArmy(const std::string &filename);
~~~

Then it is time to make the parser implementation.
First, we need to add necessary bolierplate - includes, file handling ([army_printer.cpp](https://gitlab.com/dhurum/sjparser_tutorial/blob/master/Part%201/army_printer.cpp)):

~~~cpp
#include "army_printer.h"
#include <fstream>
#include <iostream>
#include "sjparser/sjparser.h"

bool printArmy(const std::string &filename) {
  std::ifstream json(filename);

  if (!json.good()) {
    std::cout << "Failed to open '" << filename << "'\n";
    return false;
  }

  return true;
}
~~~

## Choosing the right parsers

Now let's see what we might need in order to parse king information.  
Information is an object, with two text members.

So we will need four `SJParser` entities:

1. [SJParser::Parser](https://dhurum.gitlab.io/sjparser/classSJParser_1_1Parser.html) - The main parser class;
2. [SJParser::Object](https://dhurum.gitlab.io/sjparser/classSJParser_1_1Object.html) - Parser for objects;
3. [SJParser::Member](https://dhurum.gitlab.io/sjparser/classSJParser_1_1Member.html) - Class for defining object's members;
4. [SJParser::Value](https://dhurum.gitlab.io/sjparser/classSJParser_1_1Value.html) - Parser for simple values (string, number, boolean).

In order to simplify our code we need to add `using` directives after the includes:

~~~cpp
using SJParser::Member;
using SJParser::Object;
using SJParser::Parser;
using SJParser::Value;
~~~

Now we can start defining parsers.

## Specifying JSON structure

### King information object

In SJParser you specify the json structure by passing parsers into other parsers constructors.  
You can either specify parser right in-place, or define it first and then pass to another one.  
So let's define king's object parser first:

~~~cpp
  Object king_info_parser{std::tuple{Member{"name", Value<std::string>{}},
                                     Member{"title", Value<std::string>{}}}};
~~~

Here we are saying that we have a JSON object with two members, `"name"` and `"title"`, and both of them are simple string values.

But this parser does not do anything fancy, it will just parse this object and happily stops.
In order to do something useful with what it parsed, we need to either check it's members after the parsing was finished, or specify a callback.
You can pass a callback right in the constructor, but an object callback takes a reference to the object parser, so we need to specify it's type.
In order to simplify our code we will define the callback after the object parser, and just use a setter:

~~~cpp
  bool king_info_printed = false;

  auto print_king_info = [&](decltype(king_info_parser) &parser) {
    std::cout << "King " << parser.get<0>() << ", " << parser.get<1>()
              << ", has the following army:\n";
    king_info_printed = true;

    return true;
  };

  king_info_parser.setFinishCallback(print_king_info);
~~~

As you can see, we use [SJParser::Object::get](https://dhurum.gitlab.io/sjparser/classSJParser_1_1Object.html#a3e1e2c03253f9365a938b6089da0e673) method to obtain value.
Each SJParser entity parser has at least one `get` method.
Parsers, that store parsed values (`SJParser::Value`, `SJParser::SArray`, `SJParser::SCustomObject`, `SJParser::SAutoObject`, `SJParser::SUnion`) have non-templated `get` method, which returns an lvalue to parsed value.
Also they have `pop` method, which returns an rvalue reference and marks parser as unset.

But `SJParser::Object` and `SJParser::Union` have templated `get` method, where template parameter is member's index.
If corresponding member parser stores value, it returns a reference to that value. Otherwise it returns a reference to that parser - so you do not need to write an additional `get`.
If you want to get parser, use `parser` method, every parser except `SJParser::Value` have it, either templated or not.

`SJParser::SCustomObject` has both templated and non-templated `get`.

`SJParser::SAutoObject` and `SJParser::SUnion` do not have this magical templated `get` because internally they use `pop` on their member parsers, so there is no point in getting their values - they are always unset.

Right now we can just print the king's details and be happy with it, but later we will need to check if the king's info was specified before the army, that's what the `king_info_printed` flag will be used for.

### Main parser

The object parser can not parser our JSON on it's own, the actual parsing is done via [SJParser::Parser](https://dhurum.gitlab.io/sjparser/classSJParser_1_1Parser.html) class.  
It's constructor receives one root parser and an optional argument specifying the underlying SAX parser.  
By default it is [SJParser::YajlParser](https://dhurum.gitlab.io/sjparser/classSJParser_1_1YajlParser.html)

In our case the root parser is an object, where the first member will be the king information:

~~~cpp
  Parser parser{Object{std::tuple{Member{"king", king_info_parser}}}};
~~~

## Actual parsing

So, everything is ready for the actual parsing to take place.  
It is done by calling [SJParser::YajlParser::parse](https://dhurum.gitlab.io/sjparser/classSJParser_1_1YajlParser.html#a45fb3427948d601ad22ef2967e18778b) and [SJParser::YajlParser::finish](https://dhurum.gitlab.io/sjparser/classSJParser_1_1YajlParser.html#ac745f026938ed5a337ce109ff8c0c1de) methods.

SJParser gives errors by throwing exceptions, so you need to catch `std::exception` if you want to process them.  
If you want to get more fine-grained control of the error, you can use [SJParser::ParsingError](https://dhurum.gitlab.io/sjparser/classSJParser_1_1ParsingError.html).

~~~cpp
  try {
    std::string line;
    while (std::getline(json, line)) {
      parser.parse(line);
    }
    parser.finish();
  } catch (std::exception &e) {
    std::cout << "Error while parsing " << filename << ":\n";
    std::cout << e.what() << "\n";
    return false;
  }
~~~

And that's it!

## Main file

Now we need a file with a `main` function, that will call our parser ([printarmy.cpp](https://gitlab.com/dhurum/sjparser_tutorial/blob/master/Part%201/printarmy.cpp)):

~~~cpp
#include <iostream>
#include "army_printer.h"

int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "Usage: " << argv[0] << " ARMY_FILE\n";
    return 1;
  }

  return printArmy(argv[1]) ? 0 : 1;
}
~~~

## Test JSON

Our parser can parse this json:

~~~json
{
  "king": {
    "name": "Nebuhadanazzer",
    "title": "The Great Ruler Of All Men And Beasts"
  }
}
~~~

Let's save it as [army.json](https://gitlab.com/dhurum/sjparser_tutorial/blob/master/Part%201/army.json), and test our creation.

## Testing

First, we need to create a build directory:

~~~bash
mkdir build
cd build
~~~

Now, we need to build our code:

~~~bash
cmake ../
make
~~~

Now we can test it:

~~~bash
./printarmy ../army.json
~~~

We will see this:

~~~
King Nebuhadanazzer, The Great Ruler Of All Men And Beasts, has the following army:
~~~

Awesome, the first part is done.

[Next part](https://gitlab.com/dhurum/sjparser_tutorial/tree/master/Part%202)
