#include "army_printer.h"
#include <fstream>
#include <iostream>
#include "sjparser/sjparser.h"

using SJParser::Member;
using SJParser::Object;
using SJParser::Parser;
using SJParser::Value;

bool printArmy(const std::string &filename) {
  std::ifstream json(filename);

  if (!json.good()) {
    std::cout << "Failed to open '" << filename << "'\n";
    return false;
  }

  bool king_info_printed = false;

  Object king_info_parser{std::tuple{Member{"name", Value<std::string>{}},
                                     Member{"title", Value<std::string>{}}}};

  auto print_king_info = [&](decltype(king_info_parser) &parser) {
    std::cout << "King " << parser.get<0>() << ", " << parser.get<1>()
              << ", has the following army:\n";
    king_info_printed = true;

    return true;
  };

  king_info_parser.setFinishCallback(print_king_info);

  Parser parser{Object{std::tuple{Member{"king", king_info_parser}}}};

  try {
    std::string line;
    while (std::getline(json, line)) {
      parser.parse(line);
    }
    parser.finish();
  } catch (std::exception &e) {
    std::cout << "Error while parsing " << filename << ":\n";
    std::cout << e.what() << "\n";
    return false;
  }

  return true;
}
