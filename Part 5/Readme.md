# Part 5

# Getting rid of explicit template parameters

In part 4 we created a parser class with where parsers had explicit template paremters.
It is error-prone, because if you mismatch parameters and arguments - you will get very confusing compiler errors.
And you have to repeat yourself, which is never good.

We did it because C++ 17 does not allow non const static auto members.
But is there a workaroud for this? Apparently, yes.
We can declare an auto function or an other class static mehod and use decltype on it:

~~~cpp
auto getInt() {
  return 100;
}

struct Test {
  decltype(getInt()) value = getInt();
};
~~~

There is one limitation though - this magical getter must be a function or a method of a completely class, you can not use subclasses for this.

## Writing parser getters

Whith this knowledge, let's write a class for the getters:

~~~cpp
class ArmyPrinterParsers {
  friend class ArmyPrinter;
};
~~~

Getters will be private, and in order to use them we are making `ArmyPrinter` a friend class.

### King information parser

~~~cpp
  template <typename Callback = std::nullptr_t>
  static auto getKingInfoParser(Callback callback = nullptr) {
    return Object{std::tuple{Member{"name", Value<std::string>{}},
                             Member{"title", Value<std::string>{}}},
                  callback};
  }
~~~

This static getter method is templated because we need to pass a callback to the parser.
Otherwise we would have to call `setFinishCallback` from the constructor.

### Equipment parser

The equipment parser getter receives equipment structure and a callback type as template parameters.

~~~cpp
  template <typename Equipment, typename Callback = std::nullptr_t>
  static auto getEquipmentParser(Callback callback = nullptr) {
    return SCustomObject{TypeHolder<Equipment>{},
                         std::tuple{Member{"name", Value<std::string>{}},
                                    Member{"price", Value<int64_t>{}}},
                         callback};
  }
~~~

### Award, infantryman, mage, cavalryman parsers

~~~cpp
  static auto getAwardParser() {
    return SAutoObject{std::tuple{Member{"name", Value<std::string>{}},
                                  Member{"year", Value<int64_t>{}},
                                  Member{"reason", Value<std::string>{}}}};
  }

  static auto getInfantrymanParser() {
    return Object{std::tuple{
        Member{"skills", SArray{Value<std::string>{}}, Presence::Optional}}};
  }

  static auto getMageParser() {
    return Object{std::tuple{
        Member{"spells", SArray{Value<std::string>{}}, Presence::Optional}}};
  }

  static auto getCavalrymanParser() {
    // clang-format off
    return Object{std::tuple{
      Member{"skills", SArray{Value<std::string>{}}, Presence::Optional},
      Member{"mount", Union{TypeHolder<std::string>{}, "type", std::tuple{
        Member{"horse", Object{std::tuple{
          Member{"color", Value<std::string>{}}
        }}},
        Member{"bear", Object{std::tuple{
          Member{"fangs length", Value<double>{}}
        }}},
      }}}
    }};
    // clang-format on
  }
~~~

### Unit parser

Unit parser getter receives several template parameters, and calls other getters internally:

~~~cpp
Unit parser
  template <typename Equipment,
            typename EquipmentParserCallback = std::nullptr_t,
            typename UnitParserCallback = std::nullptr_t>
  static auto getUnitParser(
      EquipmentParserCallback equipment_parser_callback = nullptr,
      UnitParserCallback unit_parser_callback = nullptr) {
    return Object{
        std::tuple{
            Member{"name", Value<std::string>{}},
            Member{"equipment", SArray{getEquipmentParser<Equipment>(
                                    equipment_parser_callback)}},
            Member{"years of service", Value<double>{}},
            Member{"officer", Value<bool>{}},
            Member{"awards", SArray{getAwardParser()}, Presence::Optional},
            Member{
                "type",
                Union{TypeHolder<std::string>{},
                      std::tuple{Member{"infantry", getInfantrymanParser()},
                                 Member{"mage", getMageParser()},
                                 Member{"cavalry", getCavalrymanParser()}}}}},
        unit_parser_callback};
  }
~~~

### Main parser

~~~cpp
  template <typename Equipment,
            typename KingInfoParserCallback = std::nullptr_t,
            typename EquipmentParserCallback = std::nullptr_t,
            typename UnitParserCallback = std::nullptr_t>
  static auto getMainParser(
      KingInfoParserCallback king_info_parser_callback = nullptr,
      EquipmentParserCallback equipment_parser_callback = nullptr,
      UnitParserCallback unit_parser_callback = nullptr) {
    return Parser{Object{std::tuple{
        Member{"king", getKingInfoParser(king_info_parser_callback)},
        Member{"army",
               Array{getUnitParser<Equipment>(equipment_parser_callback,
                                              unit_parser_callback)}}}}};
  }
~~~

## Updating ArmyPrinter

First we need some `using` declarations, to use in methods and for main parser:

~~~cpp
  using KingInfoParser = decltype(ArmyPrinterParsers::getKingInfoParser());
  using EquipmentParser =
      decltype(ArmyPrinterParsers::getEquipmentParser<Equipment>());
  using InfantrymanParser =
      decltype(ArmyPrinterParsers::getInfantrymanParser());
  using MageParser = decltype(ArmyPrinterParsers::getMageParser());
  using CavalrymanParser = decltype(ArmyPrinterParsers::getCavalrymanParser());
  using UnitParser = decltype(ArmyPrinterParsers::getUnitParser<Equipment>());
  using MainParser = decltype(ArmyPrinterParsers::getMainParser<Equipment>());
~~~

Then declare the main parser:

~~~cpp
  MainParser _parser{ArmyPrinterParsers::getMainParser<Equipment>(
      std::bind(&ArmyPrinter::printKingInfo, this, _1),
      std::bind(&ArmyPrinter::parseEquipment, this, _1, _2),
      std::bind(&ArmyPrinter::printUnit, this, _1))};
~~~

All the methods remain unmodified.

That's it!
