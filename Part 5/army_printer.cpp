#include "army_printer.h"
#include <fstream>
#include <iostream>

bool ArmyPrinter::printKingInfo(KingInfoParser &parser) {
  std::cout << "King " << parser.get<0>() << ", " << parser.get<1>()
            << ", has the following army:\n";
  _king_info_printed = true;

  return true;
}

bool ArmyPrinter::parseEquipment(EquipmentParser &parser, Equipment &value) {
  value.name = parser.pop<0>();
  value.price = parser.get<1>();

  _total_equimpent_cost += value.price;
  return true;
}

bool ArmyPrinter::printUnit(UnitParser &parser) {
  if (!_king_info_printed) {
    std::cerr << "No king info before army listing\n";
    return false;
  }

  std::cout << parser.get<0>() << ", equipped with";

  const char *separator = "";
  for (const auto &item : parser.get<1>()) {
    std::cout << separator << " a " << item.name << " (" << item.price
              << " gold)";
    separator = " and";
  }

  std::cout << ", serving you for " << parser.get<2>() << " years.";
  std::cout << " Currently " << (parser.get<3>() ? "an officer" : "a private")
            << ".";

  if (parser.parser<4>().isSet()) {
    std::cout << " Has the following awards:";

    separator = "";
    for (const auto &award : parser.get<4>()) {
      std::cout << separator << " a " << std::get<0>(award) << " for "
                << std::get<2>(award) << " in " << std::get<1>(award);
      separator = " and";
    }
    std::cout << ".";
  }

  switch (parser.get<5>().currentMemberId()) {
    case 0:
      printInfantryman(parser.get<5>().get<0>());
      break;
    case 1:
      printMage(parser.get<5>().get<1>());
      break;
    case 2:
      printCavalryman(parser.get<5>().get<2>());
      break;
    default:
      std::cerr << "Unknown unit type\n";
      return false;
  }

  std::cout << "\n";
  return true;
}

void ArmyPrinter::printSkills(const std::vector<std::string> &skills,
                              const std::string &name) {
  std::cout << " Has " << name << ":";

  const char *separator = " ";
  for (const auto &skill : skills) {
    std::cout << separator << skill;
    separator = " and ";
  }
  std::cout << ".";
}

bool ArmyPrinter::printInfantryman(InfantrymanParser &parser) {
  std::cout << " Serves as an infantryman.";

  if (parser.parser<0>().isSet()) {
    printSkills(parser.get<0>(), "skills");
  }

  return true;
}

bool ArmyPrinter::printMage(MageParser &parser) {
  std::cout << " Serves as a mage.";

  if (parser.parser<0>().isSet()) {
    printSkills(parser.get<0>(), "spells");
  }

  return true;
}

bool ArmyPrinter::printCavalryman(CavalrymanParser &parser) {
  std::cout << " Serves as a cavalryman.";

  if (parser.parser<0>().isSet()) {
    printSkills(parser.get<0>(), "skills");
  }

  std::cout << " Rides a ";
  switch (parser.get<1>().currentMemberId()) {
    case 0:
      std::cout << parser.get<1>().get<0>().get<0>() << " horse.";
      break;
    case 1:
      std::cout << "bear with the fangs " << parser.get<1>().get<1>().get<0>()
                << "mm long.";
      break;
    default:
      std::cerr << "Unknown mount type\n";
      return false;
  }

  return true;
}

bool ArmyPrinter::process(const std::string &filename) {
  std::ifstream json(filename);

  if (!json.good()) {
    std::cout << "Failed to open '" << filename << "'\n";
    return false;
  }

  _king_info_printed = false;
  _total_equimpent_cost = 0;

  try {
    std::string line;
    while (std::getline(json, line)) {
      _parser.parse(line);
    }
    _parser.finish();
  } catch (std::exception &e) {
    std::cout << "Error while parsing " << filename << ":\n";
    std::cout << e.what() << "\n";
    return false;
  }

  std::cerr << "[DEBUG] Total eqipment cost is " << _total_equimpent_cost
            << " gold\n";
  return true;
}
