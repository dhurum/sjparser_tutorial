#include <iostream>
#include "army_printer.h"

int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "Usage: " << argv[0] << " ARMY_FILE\n";
    return 1;
  }
  ArmyPrinter printer;

  return printer.process(argv[1]) ? 0 : 1;
}
