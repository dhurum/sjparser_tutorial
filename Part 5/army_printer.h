#pragma once

#include "sjparser/sjparser.h"

using SJParser::Array;
using SJParser::Member;
using SJParser::Object;
using SJParser::Parser;
using SJParser::Presence;
using SJParser::SArray;
using SJParser::SAutoObject;
using SJParser::SCustomObject;
using SJParser::TypeHolder;
using SJParser::Union;
using SJParser::Value;

using std::placeholders::_1;
using std::placeholders::_2;

class ArmyPrinterParsers {
  template <typename Callback = std::nullptr_t>
  static auto getKingInfoParser(Callback callback = nullptr) {
    return Object{std::tuple{Member{"name", Value<std::string>{}},
                             Member{"title", Value<std::string>{}}},
                  callback};
  }

  template <typename Equipment, typename Callback = std::nullptr_t>
  static auto getEquipmentParser(Callback callback = nullptr) {
    return SCustomObject{TypeHolder<Equipment>{},
                         std::tuple{Member{"name", Value<std::string>{}},
                                    Member{"price", Value<int64_t>{}}},
                         callback};
  }

  static auto getAwardParser() {
    return SAutoObject{std::tuple{Member{"name", Value<std::string>{}},
                                  Member{"year", Value<int64_t>{}},
                                  Member{"reason", Value<std::string>{}}}};
  }

  static auto getInfantrymanParser() {
    return Object{std::tuple{
        Member{"skills", SArray{Value<std::string>{}}, Presence::Optional}}};
  }

  static auto getMageParser() {
    return Object{std::tuple{
        Member{"spells", SArray{Value<std::string>{}}, Presence::Optional}}};
  }

  static auto getCavalrymanParser() {
    // clang-format off
    return Object{std::tuple{
      Member{"skills", SArray{Value<std::string>{}}, Presence::Optional},
      Member{"mount", Union{TypeHolder<std::string>{}, "type", std::tuple{
        Member{"horse", Object{std::tuple{
          Member{"color", Value<std::string>{}}
        }}},
        Member{"bear", Object{std::tuple{
          Member{"fangs length", Value<double>{}}
        }}},
      }}}
    }};
    // clang-format on
  }

  template <typename Equipment,
            typename EquipmentParserCallback = std::nullptr_t,
            typename UnitParserCallback = std::nullptr_t>
  static auto getUnitParser(
      EquipmentParserCallback equipment_parser_callback = nullptr,
      UnitParserCallback unit_parser_callback = nullptr) {
    return Object{
        std::tuple{
            Member{"name", Value<std::string>{}},
            Member{"equipment", SArray{getEquipmentParser<Equipment>(
                                    equipment_parser_callback)}},
            Member{"years of service", Value<double>{}},
            Member{"officer", Value<bool>{}},
            Member{"awards", SArray{getAwardParser()}, Presence::Optional},
            Member{
                "type",
                Union{TypeHolder<std::string>{},
                      std::tuple{Member{"infantry", getInfantrymanParser()},
                                 Member{"mage", getMageParser()},
                                 Member{"cavalry", getCavalrymanParser()}}}}},
        unit_parser_callback};
  }

  template <typename Equipment,
            typename KingInfoParserCallback = std::nullptr_t,
            typename EquipmentParserCallback = std::nullptr_t,
            typename UnitParserCallback = std::nullptr_t>
  static auto getMainParser(
      KingInfoParserCallback king_info_parser_callback = nullptr,
      EquipmentParserCallback equipment_parser_callback = nullptr,
      UnitParserCallback unit_parser_callback = nullptr) {
    return Parser{Object{std::tuple{
        Member{"king", getKingInfoParser(king_info_parser_callback)},
        Member{"army",
               Array{getUnitParser<Equipment>(equipment_parser_callback,
                                              unit_parser_callback)}}}}};
  }

  friend class ArmyPrinter;
};

class ArmyPrinter {
 public:
  bool process(const std::string &filename);  // Main method, processes json

 private:
  // Structure with unit's equipment information
  struct Equipment {
    std::string name;
    int price;
  };

  using KingInfoParser = decltype(ArmyPrinterParsers::getKingInfoParser());
  using EquipmentParser =
      decltype(ArmyPrinterParsers::getEquipmentParser<Equipment>());
  using InfantrymanParser =
      decltype(ArmyPrinterParsers::getInfantrymanParser());
  using MageParser = decltype(ArmyPrinterParsers::getMageParser());
  using CavalrymanParser = decltype(ArmyPrinterParsers::getCavalrymanParser());
  using UnitParser = decltype(ArmyPrinterParsers::getUnitParser<Equipment>());
  using MainParser = decltype(ArmyPrinterParsers::getMainParser<Equipment>());

  MainParser _parser{ArmyPrinterParsers::getMainParser<Equipment>(
      std::bind(&ArmyPrinter::printKingInfo, this, _1),
      std::bind(&ArmyPrinter::parseEquipment, this, _1, _2),
      std::bind(&ArmyPrinter::printUnit, this, _1))};

  // King information callback
  bool printKingInfo(KingInfoParser &parser);
  // Equipment information callback
  bool parseEquipment(EquipmentParser &parser, Equipment &value);
  // Unit information callback
  bool printUnit(UnitParser &parser);
  // Skill print helper
  void printSkills(const std::vector<std::string> &skills,
                   const std::string &name);
  // Infantryman print helper
  bool printInfantryman(InfantrymanParser &parser);
  // Mage print helper
  bool printMage(MageParser &parser);
  // Cavalryman print helper
  bool printCavalryman(CavalrymanParser &parser);

  bool _king_info_printed = false;
  int _total_equimpent_cost = 0;

  // clang-format on
};
