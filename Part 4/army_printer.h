#pragma once

#include "sjparser/sjparser.h"

using SJParser::Array;
using SJParser::Member;
using SJParser::Object;
using SJParser::Parser;
using SJParser::Presence;
using SJParser::SArray;
using SJParser::SAutoObject;
using SJParser::SCustomObject;
using SJParser::TypeHolder;
using SJParser::Union;
using SJParser::Value;

using std::placeholders::_1;
using std::placeholders::_2;

class ArmyPrinter {
 public:
  bool process(const std::string &filename);  // Main method, processes json

 private:
  using KingInfoParser = Object<Value<std::string>, Value<std::string>>;

  KingInfoParser _king_info_parser{
      std::tuple{Member{"name", Value<std::string>{}},
                 Member{"title", Value<std::string>{}}},
      std::bind(&ArmyPrinter::printKingInfo, this, _1)};

  // Structure with unit's equipment information
  struct Equipment {
    std::string name;
    int price;
  };

  using EquipmentParser =
      SCustomObject<Equipment, Value<std::string>, Value<int64_t>>;

  EquipmentParser _equipment_parser{
      TypeHolder<Equipment>{},
      std::tuple{Member{"name", Value<std::string>{}},
                 Member{"price", Value<int64_t>{}}},
      std::bind(&ArmyPrinter::parseEquipment, this, _1, _2)};

  using AwardParser =
      SAutoObject<Value<std::string>, Value<int64_t>, Value<std::string>>;

  AwardParser _award_parser{std::tuple{Member{"name", Value<std::string>{}},
                                       Member{"year", Value<int64_t>{}},
                                       Member{"reason", Value<std::string>{}}}};

  using InfantrymanParser = Object<SArray<Value<std::string>>>;

  InfantrymanParser _infantryman_parser{std::tuple{
      Member{"skills", SArray{Value<std::string>{}}, Presence::Optional}}};

  using MageParser = Object<SArray<Value<std::string>>>;

  MageParser _mage_parser{std::tuple{
      Member{"spells", SArray{Value<std::string>{}}, Presence::Optional}}};

  using CavalrymanParser = Object<
      SArray<Value<std::string>>,
      Union<std::string, Object<Value<std::string>>, Object<Value<double>>>>;

  // clang-format off
  CavalrymanParser _cavalryman_parser{std::tuple{
      Member{"skills", SArray{Value<std::string>{}}, Presence::Optional},
      Member{"mount", Union{TypeHolder<std::string>{}, "type", std::tuple{
        Member{"horse", Object{std::tuple{
          Member{"color", Value<std::string>{}}
        }}},
        Member{"bear", Object{std::tuple{
          Member{"fangs length", Value<double>{}}
        }}},
      }}}
    }};
  // clang-format on

  // Please, note that when you are want to pass a reference to an external
  // parser, you need to specify it as a reference in the template parameters.
  using UnitParser = Object<Value<std::string>, SArray<EquipmentParser &>,
                            Value<double>, Value<bool>, SArray<AwardParser &>,
                            Union<std::string, InfantrymanParser &,
                                  MageParser &, CavalrymanParser &>>;

  UnitParser _unit_parser{
      std::tuple{
          Member{"name", Value<std::string>{}},
          Member{"equipment", SArray{_equipment_parser}},
          Member{"years of service", Value<double>{}},
          Member{"officer", Value<bool>{}},
          Member{"awards", SArray{_award_parser}, Presence::Optional},
          Member{"type",
                 Union{TypeHolder<std::string>{},
                       std::tuple{Member{"infantry", _infantryman_parser},
                                  Member{"mage", _mage_parser},
                                  Member{"cavalry", _cavalryman_parser}}}}},
      std::bind(&ArmyPrinter::printUnit, this, _1)};

  // Please, note that when you are want to pass a reference to an external
  // parser, you need to specify it as a reference in the template parameters.
  using MainParser = Parser<Object<KingInfoParser &, Array<UnitParser &>>>;

  MainParser _parser{Object{std::tuple{Member{"king", _king_info_parser},
                                       Member{"army", Array{_unit_parser}}}}};

  // King information callback
  bool printKingInfo(KingInfoParser &parser);
  // Equipment information callback
  bool parseEquipment(EquipmentParser &parser, Equipment &value);
  // Unit information callback
  bool printUnit(UnitParser &parser);
  // Skill print helper
  void printSkills(const std::vector<std::string> &skills,
                   const std::string &name);
  // Infantryman print helper
  bool printInfantryman(InfantrymanParser &parser);
  // Mage print helper
  bool printMage(MageParser &parser);
  // Cavalryman print helper
  bool printCavalryman(CavalrymanParser &parser);

  bool _king_info_printed = false;
  int _total_equimpent_cost = 0;
};
