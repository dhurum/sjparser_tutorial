# Part 4

# Making parser class

In previous parts we made a nice parser, but it all was written in a single function.
This will not work well for larger parsers - your function will become unmanageable, or if you want to parse JSON many times - your program will waste time on initializing parsers over and over again.
The solution is to move the parser into a class, so it can be initialized once, used as many times as you want, and broken into many separate methods.

The only problem - C++ 17 allows auto class members to be only const static.
So the first natural solution would be to define parser types explicitely.

## Boilerplate

We will need a class `ArmyPrinter` with a `process` method.
Since all of our parsers will be in the class declaration, we need to move all the `using` directives to the header.
`std::placeholder` will be used in `std::bind` to pass class methods into callbacks. If you find this mechanism too slow, you can use any other solution which returns `std::function` with correct signature.
Private methods are just renamed lambdas from previous parts.
Also we will need a couple of helper members, to keep track of the internal state:

[army_printer.h](https://gitlab.com/dhurum/sjparser_tutorial/blob/master/Part%204/army_printer.h):

~~~cpp
#pragma once

#include "sjparser/sjparser.h"

using SJParser::Array;
using SJParser::Member;
using SJParser::Object;
using SJParser::Parser;
using SJParser::Presence;
using SJParser::SArray;
using SJParser::SAutoObject;
using SJParser::SCustomObject;
using SJParser::TypeHolder;
using SJParser::Union;
using SJParser::Value;

using std::placeholders::_1;
using std::placeholders::_2;

class ArmyPrinter {
 public:
  bool process(const std::string &filename);  // Main method, processes json

 private:
  // King information callback
  bool printKingInfo(KingInfoParser &parser);
  // Equipment information callback
  bool parseEquipment(EquipmentParser &parser, Equipment &value);
  // Unit information callback
  bool printUnit(UnitParser &parser);
  // Skill print helper
  void printSkills(const std::vector<std::string> &skills,
                   const std::string &name);
  // Infantryman print helper
  bool printInfantryman(InfantrymanParser &parser);
  // Mage print helper
  bool printMage(MageParser &parser);
  // Cavalryman print helper
  bool printCavalryman(CavalrymanParser &parser);

  bool _king_info_printed = false;
  int _total_equimpent_cost = 0;
};
~~~

[printarmy.cpp](https://gitlab.com/dhurum/sjparser_tutorial/blob/master/Part%204/printarmy.cpp):

~~~cpp
#include <iostream>
#include "army_printer.h"

int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "Usage: " << argv[0] << " ARMY_FILE\n";
    return 1;
  }
  ArmyPrinter printer;

  return printer.process(argv[1]) ? 0 : 1;
}
~~~

## Parsers

Now it is time to set up all the parsers in the class declaration.

### King information parser

~~~cpp
  using KingInfoParser = Object<Value<std::string>, Value<std::string>>;

  KingInfoParser _king_info_parser{
      std::tuple{Member{"name", Value<std::string>{}},
                 Member{"title", Value<std::string>{}}},
      std::bind(&ArmyPrinter::printKingInfo, this, _1)};
~~~

As you can see, first we declare parser type, and then we create a member of that type with a default value.
`std::bind` is used to pass class method as a callback.

### Equipment, awards, infantryman, cavaltyman and mage parsers

Everything is the same:

~~~cpp
  struct Equipment {
    std::string name;
    int price;
  };

  using EquipmentParser =
      SCustomObject<Equipment, Value<std::string>, Value<int64_t>>;

  EquipmentParser _equipment_parser{
      TypeHolder<Equipment>{},
      std::tuple{Member{"name", Value<std::string>{}},
                 Member{"price", Value<int64_t>{}}},
      std::bind(&ArmyPrinter::parseEquipment, this, _1, _2)};

  using AwardParser =
      SAutoObject<Value<std::string>, Value<int64_t>, Value<std::string>>;

  AwardParser _award_parser{std::tuple{Member{"name", Value<std::string>{}},
                                       Member{"year", Value<int64_t>{}},
                                       Member{"reason", Value<std::string>{}}}};

  using InfantrymanParser = Object<SArray<Value<std::string>>>;

  InfantrymanParser _infantryman_parser{std::tuple{
      Member{"skills", SArray{Value<std::string>{}}, Presence::Optional}}};

  using MageParser = Object<SArray<Value<std::string>>>;

  MageParser _mage_parser{std::tuple{
      Member{"spells", SArray{Value<std::string>{}}, Presence::Optional}}};

  using CavalrymanParser = Object<
      SArray<Value<std::string>>,
      Union<std::string, Object<Value<std::string>>, Object<Value<double>>>>;

  // clang-format off
  CavalrymanParser _cavalryman_parser{std::tuple{
      Member{"skills", SArray{Value<std::string>{}}, Presence::Optional},
      Member{"mount", Union{TypeHolder<std::string>{}, "type", std::tuple{
        Member{"horse", Object{std::tuple{
          Member{"color", Value<std::string>{}}
        }}},
        Member{"bear", Object{std::tuple{
          Member{"fangs length", Value<double>{}}
        }}},
      }}}
    }};
  // clang-format on
~~~

### Unit parser

Unit parser is special, becuase it makes use of other parsers, taking them by reference.
In order to do this, we must specify reference type in the template parameters.
Otherwise reading the resulting error message could summon Cthulhu:

~~~cpp
  using UnitParser = Object<Value<std::string>, SArray<EquipmentParser &>,
                            Value<double>, Value<bool>, SArray<AwardParser &>,
                            Union<std::string, InfantrymanParser &,
                                  MageParser &, CavalrymanParser &>>;

  UnitParser _unit_parser{
      std::tuple{
          Member{"name", Value<std::string>{}},
          Member{"equipment", SArray{_equipment_parser}},
          Member{"years of service", Value<double>{}},
          Member{"officer", Value<bool>{}},
          Member{"awards", SArray{_award_parser}, Presence::Optional},
          Member{"type",
                 Union{TypeHolder<std::string>{},
                       std::tuple{Member{"infantry", _infantryman_parser},
                                  Member{"mage", _mage_parser},
                                  Member{"cavalry", _cavalryman_parser}}}}},
      std::bind(&ArmyPrinter::printUnit, this, _1)};
~~~

### Main parser

In the main parser we use reference types as well:

~~~cpp
  using MainParser = Parser<Object<KingInfoParser &, Array<UnitParser &>>>;

  MainParser _parser{Object{std::tuple{Member{"king", _king_info_parser},
                                       Member{"army", Array{_unit_parser}}}}};
~~~

## Methods implementation

Implementation of methods is taken directly from lambdas from the previous part.
The only different part is `ArmyPrinter::process`:

~~~cpp
bool ArmyPrinter::process(const std::string &filename) {
  std::ifstream json(filename);

  if (!json.good()) {
    std::cout << "Failed to open '" << filename << "'\n";
    return false;
  }

  _king_info_printed = false;
  _total_equimpent_cost = 0;

  try {
    std::string line;
    while (std::getline(json, line)) {
      _parser.parse(line);
    }
    _parser.finish();
  } catch (std::exception &e) {
    std::cout << "Error while parsing " << filename << ":\n";
    std::cout << e.what() << "\n";
    return false;
  }

  std::cerr << "[DEBUG] Total eqipment cost is " << _total_equimpent_cost
            << " gold\n";
  return true;
}
~~~

The rest you can find in [army_printer.cpp](https://gitlab.com/dhurum/sjparser_tutorial/blob/master/Part%204/army_printer.cpp).

Good, now we have a parser class. But parsers declarations use explicit template parameters, and this is error-prone.

[Next part](https://gitlab.com/dhurum/sjparser_tutorial/tree/master/Part%205)
