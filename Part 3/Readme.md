# Part 3

# Specialized unit information parsing

## Choosing the right parsers

Now it is time to parse specialized unit information. In order to do it, we need to utilize [SJParser::Union](https://dhurum.gitlab.io/sjparser/classSJParser_1_1Union.html). It allows you to parse an object depending on a value of one of the members. There are two kinds of unions, standalone ones where the type member must be the first one, and embedded ones, where the beginning of the object is a conventional [SJParser::Object](https://dhurum.gitlab.io/sjparser/classSJParser_1_1Object.html), and the variable part comes starting from some member.

So here is the `using` directive:

~~~cpp
using SJParser::Union;
~~~

## Specifying she JSON structure

First we need to define specialized parsers for different unit types.

### Infantryman object

A simple object, with optional array of strings:

~~~cpp
  Object infantryman_parser{std::tuple{
      Member{"skills", SArray{Value<std::string>{}}, Presence::Optional}}};
~~~

### Mage object

Similar object:

~~~cpp
  Object mage_parser{std::tuple{
      Member{"spells", SArray{Value<std::string>{}}, Presence::Optional}}};
~~~

### Cavalryman object

Caralryman is complicated, it needs another union, for mount information. We need a standalone union for that.
In order to define a standalone union, just pass type member name into it's constructor:

~~~cpp
  // clang-format off
  Object cavalryman_parser{std::tuple{
    Member{"skills", SArray{Value<std::string>{}}, Presence::Optional},
    Member{"mount", Union{TypeHolder<std::string>{}, "type", std::tuple{
      Member{"horse", Object{std::tuple{
        Member{"color", Value<std::string>{}}
      }}},
      Member{"bear", Object{std::tuple{
        Member{"fangs length", Value<double>{}}
      }}},
    }}}
  }};
  // clang-format on
~~~

As you can see, `SJParser::Union` takes type of the type member (string in our case), and for this particular one that member is called "type".

Also you might notice the `// clang-format off` / `// clang-format on` comments. Sometimes clang does not produce pretty formatting, so by using these comments we can prevent it from formatting a piece of code.

### Unit parser

Now let's modify unit parser:

~~~cpp
  Object unit_parser{std::tuple{
      Member{"name", Value<std::string>{}},
      Member{"equipment", SArray{equipment_parser}},
      Member{"years of service", Value<double>{}},
      Member{"officer", Value<bool>{}}, Member{"awards", SArray{award_parser}},
      Member{"type", Union{TypeHolder<std::string>{},
                           std::tuple{Member{"infantry", infantryman_parser},
                                      Member{"mage", mage_parser},
                                      Member{"cavalry", cavalryman_parser}}}}}};
~~~

As you can see, we are using an embedded `SJParser::Union` now. You still need to pass type member type to it's constructor, but the member name is unnecessary - it is parsed by enclosing `SJParser::Object`.

## Helper functions

### Skills printer

All of our units have skills (or spells). Let's make a small helper to print them:

~~~cpp
namespace {
void printSkills(const std::vector<std::string> &skills,
                 const std::string &name) {
  std::cout << " Has " << name << ":";

  const char *separator = " ";
  for (const auto &skill : skills) {
    std::cout << separator << skill;
    separator = " and ";
  }
  std::cout << ".";
}
}  // namespace
~~~

### Infantryman printer

A very simple lambda:

~~~cpp
  // Not a callback, just a helper lambda
  auto print_infantryman = [](decltype(infantryman_parser) &parser) {
    std::cout << " Serves as an infantryman.";

    if (parser.parser<0>().isSet()) {
      printSkills(parser.get<0>(), "skills");
    }

    return true;
  };
~~~

### Mage printer

Almost the same:

~~~cpp
  // Not a callback, just a helper lambda
  auto print_mage = [](decltype(mage_parser) &parser) {
    std::cout << " Serves as a mage.";

    if (parser.parser<0>().isSet()) {
      printSkills(parser.get<0>(), "spells");
    }

    return true;
  };
~~~

### Cavalryman printer

Now for cavalryman we need to work with `SJParser::Union`. In order to understand, what member was actually parser, we need to use [SJParser::Union::currentMemberId](https://dhurum.gitlab.io/sjparser/classSJParser_1_1Union.html#acdb88d803189a9df22f0c7dc8630138d):

~~~cpp
  // Not a callback, just a helper lambda
  auto print_cavalryman = [](decltype(cavalryman_parser) &parser) {
    std::cout << " Serves as a cavalryman.";

    if (parser.parser<0>().isSet()) {
      printSkills(parser.get<0>(), "skills");
    }

    std::cout << " Rides a ";
    switch (parser.get<1>().currentMemberId()) {
      case 0:
        std::cout << parser.get<1>().get<0>().get<0>() << " horse.";
        break;
      case 1:
        std::cout << "bear with the fangs " << parser.get<1>().get<1>().get<0>()
                  << "mm long.";
        break;
      default:
        std::cerr << "Unknown mount type" << std::endl;
        return false;
    }

    return true;
  };
~~~

Here, as you can see, `parser.get<1>()` returns the union parser, `parser.get<1>().get<0>()` returns first object in union, and `parser.get<1>().get<0>().get<0>()` returns first member of that object.

### Unit parser

Now we need to modify unit parser to support all these goodies:

~~~cpp
    switch (parser.get<5>().currentMemberId()) {
      case 0:
        print_infantryman(parser.get<5>().get<0>());
        break;
      case 1:
        print_mage(parser.get<5>().get<1>());
        break;
      case 2:
        print_cavalryman(parser.get<5>().get<2>());
        break;
      default:
        std::cerr << "Unknown unit type\n";
        return false;
    }
~~~

And we are done with parsers!

## Test JSON

All that's left to do now is to update the [army.json](https://gitlab.com/dhurum/sjparser_tutorial/blob/master/Part%203/army.json) and check out program:

~~~json
{
  "king": {
    "name": "Nebuhadanazzer",
    "title": "The Great Ruler Of All Men And Beasts"
  },
  "army": [{
    "name": "Gork",
    "equipment": [{
      "name": "sword",
      "price": 10
    }, {
      "name": "shield",
      "price": 15
    }],
    "years of service": 5.2,
    "officer": true,
    "awards": [{
      "name": "Medal of Glory",
      "year": 967,
      "reason": "Exceptional courage at the Gondolin battle"
    },{
      "name": "Medal of Victory",
      "year": 968,
      "reason": "Victory in the Lorien conflict"
    }],
    "type": "infantry",
    "skills": ["Powerful strike"]
  }, {
    "name": "Mork",
    "equipment": [{
      "name": "staff",
      "price": 8
    }],
    "years of service": 4.0,
    "officer": false,
    "type": "mage",
    "spells": ["Shakles of ice", "Ring of fire", "Rain of steel"]
  }, {
    "name": "Snaga",
    "equipment": [{
      "name": "pike",
      "price": 14
    }],
    "years of service": 3.1,
    "officer": true,
    "type": "cavalry",
    "skills": ["Charge"],
    "mount": {
      "type": "horse",
      "color": "white"
    }
  }, {
    "name": "Ugluk",
    "equipment": [{
      "name": "saber",
      "price": 9
    }],
    "years of service": 1.6,
    "officer": false,
    "awards": [{
      "name": "Medal of Victory",
      "year": 968,
      "reason": "Victory in the Lorien conflict"
    }],
    "type": "cavalry",
    "mount": {
      "type": "bear",
      "fangs length": 10.2
    }
  }]
}
~~~

~~~bash
make
./printarmy ../army.json
~~~

And the final result:

~~~
King Nebuhadanazzer, The Great Ruler Of All Men And Beasts, has the following army:
Gork, equipped with a sword (10 gold) and a shield (15 gold), serving you for 5.2 years. Currently an officer. Has the following awards: a Medal of Glory for Exceptional courage at the Gondolin battle in 967 and a Medal of Victory for Victory in the Lorien conflict in 968. Serves as an infantryman. Has skills: Powerful strike.
Mork, equipped with a staff (8 gold), serving you for 4 years. Currently a private. Serves as a mage. Has spells: Shakles of ice and Ring of fire and Rain of steel.
Snaga, equipped with a pike (14 gold), serving you for 3.1 years. Currently an officer. Serves as a cavalryman. Has skills: Charge. Rides a white horse.
Ugluk, equipped with a saber (9 gold), serving you for 1.6 years. Currently a private. Has the following awards: a Medal of Victory for Victory in the Lorien conflict in 968. Serves as a cavalryman. Rides a bear with the fangs 10.2mm long.
[DEBUG] Total eqipment cost is 56 gold
~~~

There is a problem though - our parser is inside a function, which is not very scalable.

[Next part](https://gitlab.com/dhurum/sjparser_tutorial/tree/master/Part%204)
