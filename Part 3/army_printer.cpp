#include <fstream>
#include <iostream>
#include "army_printer.h"
#include "sjparser/sjparser.h"

using SJParser::Array;
using SJParser::Member;
using SJParser::Object;
using SJParser::Parser;
using SJParser::Presence;
using SJParser::SArray;
using SJParser::SAutoObject;
using SJParser::SCustomObject;
using SJParser::TypeHolder;
using SJParser::Union;
using SJParser::Value;

namespace {
void printSkills(const std::vector<std::string> &skills,
                 const std::string &name) {
  std::cout << " Has " << name << ":";

  const char *separator = " ";
  for (const auto &skill : skills) {
    std::cout << separator << skill;
    separator = " and ";
  }
  std::cout << ".";
}
}  // namespace

bool printArmy(const std::string &filename) {
  std::ifstream json(filename);

  if (!json.good()) {
    std::cout << "Failed to open '" << filename << "'\n";
    return false;
  }

  bool king_info_printed = false;
  int total_equimpent_cost = 0;

  Object king_info_parser{std::tuple{Member{"name", Value<std::string>{}},
                                     Member{"title", Value<std::string>{}}}};

  auto print_king_info = [&](decltype(king_info_parser) &parser) {
    std::cout << "King " << parser.get<0>() << ", " << parser.get<1>()
              << ", has the following army:\n";
    king_info_printed = true;

    return true;
  };

  king_info_parser.setFinishCallback(print_king_info);

  struct Equipment {
    std::string name;
    int price;
  };

  SCustomObject equipment_parser{
      TypeHolder<Equipment>{}, std::tuple{Member{"name", Value<std::string>{}},
                                          Member{"price", Value<int64_t>{}}}};

  auto parse_equipment = [&](decltype(equipment_parser) &parser,
                             Equipment &value) {
    value.name = parser.pop<0>();
    value.price = parser.get<1>();

    total_equimpent_cost += value.price;
    return true;
  };

  equipment_parser.setFinishCallback(parse_equipment);

  SAutoObject award_parser{std::tuple{Member{"name", Value<std::string>{}},
                                      Member{"year", Value<int64_t>{}},
                                      Member{"reason", Value<std::string>{}}}};

  Object infantryman_parser{std::tuple{
      Member{"skills", SArray{Value<std::string>{}}, Presence::Optional}}};

  Object mage_parser{std::tuple{
      Member{"spells", SArray{Value<std::string>{}}, Presence::Optional}}};

  // clang-format off
  Object cavalryman_parser{std::tuple{
    Member{"skills", SArray{Value<std::string>{}}, Presence::Optional},
    Member{"mount", Union{TypeHolder<std::string>{}, "type", std::tuple{
      Member{"horse", Object{std::tuple{
        Member{"color", Value<std::string>{}}
      }}},
      Member{"bear", Object{std::tuple{
        Member{"fangs length", Value<double>{}}
      }}},
    }}}
  }};
  // clang-format on

  Object unit_parser{std::tuple{
      Member{"name", Value<std::string>{}},
      Member{"equipment", SArray{equipment_parser}},
      Member{"years of service", Value<double>{}},
      Member{"officer", Value<bool>{}},
      Member{"awards", SArray{award_parser}, Presence::Optional},
      Member{"type", Union{TypeHolder<std::string>{},
                           std::tuple{Member{"infantry", infantryman_parser},
                                      Member{"mage", mage_parser},
                                      Member{"cavalry", cavalryman_parser}}}}}};

  // Not a callback, just a helper lambda
  auto print_infantryman = [](decltype(infantryman_parser) &parser) {
    std::cout << " Serves as an infantryman.";

    if (parser.parser<0>().isSet()) {
      printSkills(parser.get<0>(), "skills");
    }

    return true;
  };

  // Not a callback, just a helper lambda
  auto print_mage = [](decltype(mage_parser) &parser) {
    std::cout << " Serves as a mage.";

    if (parser.parser<0>().isSet()) {
      printSkills(parser.get<0>(), "spells");
    }

    return true;
  };

  // Not a callback, just a helper lambda
  auto print_cavalryman = [](decltype(cavalryman_parser) &parser) {
    std::cout << " Serves as a cavalryman.";

    if (parser.parser<0>().isSet()) {
      printSkills(parser.get<0>(), "skills");
    }

    std::cout << " Rides a ";
    switch (parser.get<1>().currentMemberId()) {
      case 0:
        std::cout << parser.get<1>().get<0>().get<0>() << " horse.";
        break;
      case 1:
        std::cout << "bear with the fangs " << parser.get<1>().get<1>().get<0>()
                  << "mm long.";
        break;
      default:
        std::cerr << "Unknown mount type" << std::endl;
        return false;
    }

    return true;
  };

  auto print_unit = [&](decltype(unit_parser) &parser) {
    if (!king_info_printed) {
      std::cerr << "No king info before army listing\n";
      return false;
    }

    std::cout << parser.get<0>() << ", equipped with";

    const char *separator = "";
    for (const auto &item : parser.get<1>()) {
      std::cout << separator << " a " << item.name << " (" << item.price
                << " gold)";
      separator = " and";
    }

    std::cout << ", serving you for " << parser.get<2>() << " years.";
    std::cout << " Currently " << (parser.get<3>() ? "an officer" : "a private")
              << ".";

    if (parser.parser<4>().isSet()) {
      std::cout << " Has the following awards:";

      separator = "";
      for (const auto &award : parser.get<4>()) {
        std::cout << separator << " a " << std::get<0>(award) << " for "
                  << std::get<2>(award) << " in " << std::get<1>(award);
        separator = " and";
      }
      std::cout << ".";
    }

    switch (parser.get<5>().currentMemberId()) {
      case 0:
        print_infantryman(parser.get<5>().get<0>());
        break;
      case 1:
        print_mage(parser.get<5>().get<1>());
        break;
      case 2:
        print_cavalryman(parser.get<5>().get<2>());
        break;
      default:
        std::cerr << "Unknown unit type\n";
        return false;
    }

    std::cout << "\n";
    return true;
  };

  unit_parser.setFinishCallback(print_unit);

  Parser parser{Object{std::tuple{Member{"king", king_info_parser},
                                  Member{"army", Array{unit_parser}}}}};

  try {
    std::string line;
    while (std::getline(json, line)) {
      parser.parse(line);
    }
    parser.finish();
  } catch (std::exception &e) {
    std::cout << "Error while parsing " << filename << ":\n";
    std::cout << e.what() << "\n";
    return false;
  }

  std::cerr << "[DEBUG] Total eqipment cost is " << total_equimpent_cost
            << " gold\n";
  return true;
}
