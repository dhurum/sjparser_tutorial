#include <fstream>
#include <iostream>
#include "army_printer.h"
#include "sjparser/sjparser.h"

using SJParser::Array;
using SJParser::Member;
using SJParser::Object;
using SJParser::Parser;
using SJParser::Presence;
using SJParser::SArray;
using SJParser::SAutoObject;
using SJParser::SCustomObject;
using SJParser::TypeHolder;
using SJParser::Value;

bool printArmy(const std::string &filename) {
  std::ifstream json(filename);

  if (!json.good()) {
    std::cout << "Failed to open '" << filename << "'\n";
    return false;
  }

  bool king_info_printed = false;
  int total_equimpent_cost = 0;

  Object king_info_parser{std::tuple{Member{"name", Value<std::string>{}},
                                     Member{"title", Value<std::string>{}}}};

  auto print_king_info = [&](decltype(king_info_parser) &parser) {
    std::cout << "King " << parser.get<0>() << ", " << parser.get<1>()
              << ", has the following army:\n";
    king_info_printed = true;

    return true;
  };

  king_info_parser.setFinishCallback(print_king_info);

  struct Equipment {
    std::string name;
    int price;
  };

  SCustomObject equipment_parser{
      TypeHolder<Equipment>{}, std::tuple{Member{"name", Value<std::string>{}},
                                          Member{"price", Value<int64_t>{}}}};

  auto parse_equipment = [&](decltype(equipment_parser) &parser,
                             Equipment &value) {
    value.name = parser.pop<0>();
    value.price = parser.get<1>();

    total_equimpent_cost += value.price;
    return true;
  };

  equipment_parser.setFinishCallback(parse_equipment);

  SAutoObject award_parser{std::tuple{Member{"name", Value<std::string>{}},
                                      Member{"year", Value<int64_t>{}},
                                      Member{"reason", Value<std::string>{}}}};

  Object unit_parser{
      std::tuple{Member{"name", Value<std::string>{}},
                 Member{"equipment", SArray{equipment_parser}},
                 Member{"years of service", Value<double>{}},
                 Member{"officer", Value<bool>{}},
                 Member{"awards", SArray{award_parser}, Presence::Optional}}};

  auto print_unit = [&](decltype(unit_parser) &parser) {
    if (!king_info_printed) {
      std::cerr << "No king info before army listing\n";
      return false;
    }

    std::cout << parser.get<0>() << ", equipped with";

    const char *separator = "";
    for (const auto &item : parser.get<1>()) {
      std::cout << separator << " a " << item.name << " (" << item.price
                << " gold)";
      separator = " and";
    }

    std::cout << ", serving you for " << parser.get<2>() << " years.";
    std::cout << " Currently " << (parser.get<3>() ? "an officer" : "a private")
              << ".";

    if (parser.parser<4>().isSet()) {
      std::cout << " Has the following awards:";

      separator = "";
      for (const auto &award : parser.get<4>()) {
        std::cout << separator << " a " << std::get<0>(award) << " for "
                  << std::get<2>(award) << " in " << std::get<1>(award);
        separator = " and";
      }
      std::cout << ".";
    }

    std::cout << "\n";
    return true;
  };

  unit_parser.setFinishCallback(print_unit);

  Parser parser{Object{std::tuple{Member{"king", king_info_parser},
                                  Member{"army", Array{unit_parser}}}}};

  try {
    std::string line;
    while (std::getline(json, line)) {
      parser.parse(line);
    }
    parser.finish();
  } catch (std::exception &e) {
    std::cout << "Error while parsing " << filename << ":\n";
    std::cout << e.what() << "\n";
    return false;
  }

  std::cerr << "[DEBUG] Total eqipment cost is " << total_equimpent_cost
            << " gold\n";
  return true;
}
