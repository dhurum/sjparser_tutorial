# Part 2

# Common unit information parsing

Unit is pretty complicated object, so let's write a parser for it in two stages: first the common part, and then the specialized ones.

## Choosing the right parsers

Unit informartion is an object, and it is stored in an array. Unit consists of several small objects and arrays of objects, so it would make sense to use storage array and object parsers.
For demonstration purpose we will use both [SJParser::SCustomObject](https://dhurum.gitlab.io/sjparser/classSJParser_1_1SCustomObject.html) and [SJParser::SAutoObject](https://dhurum.gitlab.io/sjparser/classSJParser_1_1SAutoObject.html).
`SJParser::SAutoObject` stores parsed result in `std::tuple`, and `SJParser::SCustomObject` stores it in a structure or an object. In order to pass this structure or object type we need [SJParser::TypeHolder](https://dhurum.gitlab.io/sjparser/structSJParser_1_1TypeHolder.html).
Also, we will use [SJParser::Array](https://dhurum.gitlab.io/sjparser/classSJParser_1_1SArray.html) and [SJParser::SArray](https://dhurum.gitlab.io/sjparser/classSJParser_1_1Array.html).
Unit has an optional member (awards), so we will need also [SJParser::Presence](https://dhurum.gitlab.io/sjparser/namespaceSJParser.html#a516ef3d2a878f243ee84cff0f2516390).

So, let's add `using` directives for them:

~~~cpp
using SJParser::Array;
using SJParser::SArray;
using SJParser::SAutoObject;
using SJParser::SCustomObject;
using SJParser::TypeHolder;
using SJParser::Presence;
~~~

Now we can use them to define our JSON structure further.

## Specifying JSON structure

### Equipment object

Equipment will be stored it in a structure:

~~~cpp
  struct Equipment {
    std::string name;
    int price;
  };
~~~

So naturally we need `SJParser::SCustomObject`. Notice, how `SJParser::TypeHolder`is used:

~~~cpp
  SCustomObject equipment_parser{
      TypeHolder<Equipment>{}, std::tuple{Member{"name", Value<std::string>{}},
                                          Member{"price", Value<int64_t>{}}}};
~~~

Unfortunately, SJParser can not automatically fill this kind of custom structures with values, so we must define a finish callback and do all the dirty work ourselfes. Meanwhile we can calculate total equipment cost, just in case our dear king looks at the list and asks "I wonder how much did I spend on arming all these lads":

~~~cpp
  int total_equimpent_cost = 0;

  auto parse_equipment = [&](decltype(equipment_parser) &parser,
                             Equipment &value) {
    value.name = parser.pop<0>();
    value.price = parser.get<1>();

    total_equimpent_cost += value.price;
    return true;
  };

  equipment_parser.setFinishCallback(parse_equipment);
~~~

As you can see, in the finish callback we setup the stored value, which can be retrieved from parser later.
Why can't we use the parsed values directly, why bother with setting parser's internal value?
Well, this can be very useful if we use this object inside of `SJParser::SArray` which will produce a nice `std::vector<Equipment>`, as you will see very soon.

### Award object

Award is a simple object, so let's use the power of `SJParser::SAutoObject` in order parse and store it:

~~~cpp
  SAutoObject award_parser{std::tuple{Member{"name", Value<std::string>{}},
                                      Member{"year", Value<int64_t>{}},
                                      Member{"reason", Value<std::string>{}}}};
~~~

### Unit object

Unit's common part is a complicated object, with some simple members and arrays of equipment and awards.
We will use `SJParser::SArray` to process those arrays, so we don't have to process their elements manually.
Awards are optional, so we need to mark the member with `SJParser::Presence::Optional`:

~~~cpp
  Object unit_parser{
      std::tuple{Member{"name", Value<std::string>{}},
                 Member{"equipment", SArray{equipment_parser}},
                 Member{"years of service", Value<double>{}},
                 Member{"officer", Value<bool>{}},
                 Member{"awards", SArray{award_parser}, Presence::Optional}}};
~~~

Now we need to make a callback, so we can print unit information.
First we need to check if the king's information is present - kings always get mad if they don't see their name in the beginning of an official document: 

~~~cpp
  auto print_unit = [&](decltype(unit_parser) &parser) {
    if (!king_info_printed) {
      std::cerr << "No king info before army listing\n";
      return false;
    }
~~~

Now let's print unit's name:

~~~cpp
    std::cout << parser.get<0>() << ", equipped with";
~~~

And now we need to print equipment. Since it is stored in an `std::vector<Equipment>` it is trivial:

~~~cpp
    const char *separator = "";
    for (const auto &item : parser.get<1>()) {
      std::cout << separator << " a " << item.name << " (" << item.price
                << " gold)";
      separator = " and";
    }
~~~


Then we print years of service and officer status:

~~~cpp
    std::cout << ", serving you for " << parser.get<2>() << " years.";
    std::cout << " Currently " << (parser.get<3>() ? "an officer" : "a private")
              << ".";
~~~

Then we need to print awards information. It is an optional member, so before we do it, we need to check if it was present in the json. After that we just get value of [SJParser::SArray](https://dhurum.gitlab.io/sjparser/classSJParser_1_1SArray.html) which is, in this case, `std::vector<std::tuple>`:

~~~cpp
    if (parser.parser<4>().isSet()) {
      std::cout << " Has the following awards:";

      separator = "";
      for (const auto &award : parser.get<4>()) {
        std::cout << separator << " a " << std::get<0>(award) << " for "
                  << std::get<2>(award) << " in " << std::get<1>(award);
        separator = " and";
      }
      std::cout << ".";
    }
~~~

And just a finishing touch:

~~~cpp

    std::cout << "\n";
    return true;
  };

  unit_parser.setFinishCallback(print_unit);
~~~

### Main parser

Now we need to add the array of units to the main parser:

~~~cpp
  Parser parser{Object{std::tuple{Member{"king", king_info_parser},
                                  Member{"army", Array{unit_parser}}}}};
~~~

And let's add debug output of total equipment cost to the very end of `printArmy`:

~~~cpp
  std::cerr << "[DEBUG] Total eqipment cost is " << total_equimpent_cost
            << " gold\n";
~~~

Great, we are done!

## Test JSON

Let's update [army.json](https://gitlab.com/dhurum/sjparser_tutorial/blob/master/Part%202/army.json) and check the results:

~~~json
{
  "king": {
    "name": "Nebuhadanazzer",
    "title": "The Great Ruler Of All Men And Beasts"
  },
  "army": [{
    "name": "Gork",
    "equipment": [{
      "name": "sword",
      "price": 10
    }, {
      "name": "shield",
      "price": 15
    }],
    "years of service": 5.2,
    "officer": true,
    "awards": [{
      "name": "Medal of Glory",
      "year": 967,
      "reason": "Exceptional courage at the Gondolin battle"
    },{
      "name": "Medal of Victory",
      "year": 968,
      "reason": "Victory in the Lorien conflict"
    }]
  }, {
    "name": "Mork",
    "equipment": [{
      "name": "staff",
      "price": 8
    }],
    "years of service": 4.0,
    "officer": false
  }, {
    "name": "Snaga",
    "equipment": [{
      "name": "pike",
      "price": 14
    }],
    "years of service": 3.1,
    "officer": true
  }, {
    "name": "Ugluk",
    "equipment": [{
      "name": "saber",
      "price": 9
    }],
    "years of service": 1.6,
    "officer": false,
    "awards": [{
      "name": "Medal of Victory",
      "year": 968,
      "reason": "Victory in the Lorien conflict"
    }]
  }]
}
~~~

## Testing

~~~bash
make
./printarmy ../army.json
~~~

Now we see this:

~~~
King Nebuhadanazzer, The Great Ruler Of All Men And Beasts, has the following army:
Gork, equipped with a sword (10 gold) and a shield (15 gold), serving you for 5.2 years. Currently an officer. Has the following awards: a Medal of Glory for Exceptional courage at the Gondolin battle in 967 and a Medal of Victory for Victory in the Lorien conflict in 968.
Mork, equipped with a staff (8 gold), serving you for 4 years. Currently a private.
Snaga, equipped with a pike (14 gold), serving you for 3.1 years. Currently an officer.
Ugluk, equipped with a saber (9 gold), serving you for 1.6 years. Currently a private. Has the following awards: a Medal of Victory for Victory in the Lorien conflict in 968.
[DEBUG] Total eqipment cost is 56 gold
~~~

The second part is done.

[Next part](https://gitlab.com/dhurum/sjparser_tutorial/tree/master/Part%203)
